import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { ActivityIndicator, Text, View } from "react-native";
import LandingScreen from "./components/auth/Landing";
import RegisterScreen from "./components/auth/Register";
import * as firebase from "firebase";
import { useEffect } from "react";
import { LinearGradient } from "expo-linear-gradient";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import rootReducer from "./components/redux/reducers";
import thunk from "redux-thunk";
const store = createStore(rootReducer, applyMiddleware(thunk));
import MainScreen from "./components/auth/Main";
import SaveScreen from "./components/main/Save";

import AddScreen from "./components/main/Add";

const firebaseConfig = {
  apiKey: "AIzaSyAiU8PXBQKbxM79JeKj9gjzflBB9s8J5Rk",
  authDomain: "clonestagram-dev-2b528.firebaseapp.com",
  projectId: "clonestagram-dev-2b528",
  storageBucket: "clonestagram-dev-2b528.appspot.com",
  messagingSenderId: "724780905123",
  appId: "1:724780905123:web:8224701271468458d34516",
  measurementId: "G-06WDE62Y11",
};

//make sure not running any firebase instance at the moment
if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

const Stack = createStackNavigator();

export default function App(props) {
  const [loggedIn, setLoggedIn] = useState(false);
  const [loaded, setLoaded] = useState(false);

  // firebase.auth().signOut();

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      // console.log(user);
      if (!user) {
        setLoggedIn(false);
        setLoaded(true);
      } else {
        setLoggedIn(true);
        setLoaded(true);
      }
    });
  }, []);

  if (!loaded) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator />
        <Text>Loading...</Text>
      </View>
    );
  }

  if (!loggedIn) {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator inititalRouteName="Main">
            <Stack.Screen
              name="Main"
              component={MainScreen}
              options={{ headerShown: false }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Main">
          <Stack.Screen
            name="Main"
            component={MainScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Add"
            component={AddScreen}
            navigation={props.navigation}
          />
          <Stack.Screen
            name="Save"
            component={SaveScreen}
            navigation={props.navigation}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
