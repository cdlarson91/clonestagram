import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StatusBar, Text, View } from "react-native";
import { Button } from "react-native-elements";

export default function Landing({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      <StatusBar />
      <LinearGradient
        colors={["#3E205B", "#693699"]}
        style={{
          flex: 1,
          width: "100%",
          height: "100%",
          opacity: 1,
          justifyContent: "center",
          alignItems: "center",
          position: "absolute",
        }}
        start={{ x: 0.5, y: 0 }}
        end={{ x: 0.5, y: 1 }}
      >
        <View
          style={{
            // flex: 1,
            height: "90%",
            width: "80%",
            borderRadius: 30,
            backgroundColor: "white",
            justifyContent: "center",
            // alignItems: "center",
            elevation: 10,
          }}
          opacity={0.9}
        >
          <Button
            buttonStyle={{ width: "50%", alignSelf: "center", margin: 2 }}
            title="Register"
            onPress={() => navigation.navigate("Register")}
          />
          <Button
            buttonStyle={{ width: "50%", alignSelf: "center", margin: 2 }}
            title="Login"
            onPress={() => navigation.navigate("Login")}
          />
        </View>
      </LinearGradient>
    </View>
  );
}
