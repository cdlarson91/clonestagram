import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { useEffect } from "react";
import { StatusBar, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { fetchUser } from "../redux/actions";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import FeedScreen from "../main/Feed";
import ProfileScreen from "../main/Profile";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const EmptyScreen = () => {
  return null;
};

export default function Main({ navigation }) {
  const userState = useSelector((state) => state.userState);
  const { currentUser } = userState;
  const dispatch = useDispatch();

  const Tab = createMaterialBottomTabNavigator();

  useEffect(() => {
    fetchUser(dispatch);
  }, []);

  if (currentUser == undefined) {
    return (
      <View
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      ></View>
    );
  } else {
    return (
      <Tab.Navigator initialRouteName="Feed" labeled={false}>
        <Tab.Screen
          name="Feed"
          component={FeedScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={26} />
            ),
          }}
        />

        <Tab.Screen
          name="AddContainer" //needs to be something other than "Add" otherwise it will navigate to the "Add" instead of going through the app.js navigation container
          listeners={({ navigation }) => ({
            tabPress: (event) => {
              event.preventDefault();
              navigation.navigate("Add");
            },
          })}
          component={EmptyScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="plus-box" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Profile"
          component={ProfileScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="account-circle"
                color={color}
                size={26}
              />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }
}
