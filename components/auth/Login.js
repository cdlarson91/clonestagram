import React, { useState } from "react";
import { Button, TextInput, View } from "react-native";
import * as firebase from "firebase";

export default function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onLogin = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <View>
      <TextInput
        placeholder="email"
        onChangeText={(input) => setEmail(input)}
      />
      <TextInput
        placeholder="password"
        secureTextEntry={true}
        onChangeText={(input) => setPassword(input)}
      />
      <Button title="Sign In" onPress={onLogin} />
    </View>
  );
}
