import React, { useState } from "react";
import { View } from "react-native";
import * as firebase from "firebase";
import { LinearGradient } from "expo-linear-gradient";
import { Button, Input } from "react-native-elements";

export default function Register({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");

  const onSignUp = () => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        firebase
          .firestore()
          .collection("users")
          .doc(firebase.auth().currentUser.uid)
          .set({ name, email });
        console.log(result);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      <LinearGradient
        colors={["#3E205B", "#693699"]}
        style={{
          flex: 1,
          width: "100%",
          height: "100%",
          opacity: 1,
          justifyContent: "center",
          alignItems: "center",
          position: "absolute",
        }}
        start={{ x: 0.5, y: 0 }}
        end={{ x: 0.5, y: 1 }}
      >
        <View
          style={{
            // flex: 1,
            height: "90%",
            width: "80%",
            borderRadius: 30,
            backgroundColor: "transparent",
            justifyContent: "center",
            // alignItems: "center",
            elevation: 10,
          }}
          opacity={0.9}
        >
          <Input
            containerStyle={{ width: "50%", alignSelf: "center" }}
            inputStyle={{ color: "white" }}
            placeholder="name"
            onChangeText={(input) => setName(input)}
          />
          <Input
            containerStyle={{ width: "50%", alignSelf: "center" }}
            inputStyle={{ color: "white" }}
            placeholder="email"
            onChangeText={(input) => setEmail(input)}
          />
          <Input
            containerStyle={{ width: "50%", alignSelf: "center" }}
            inputStyle={{ color: "white" }}
            placeholder="password"
            secureTextEntry={true}
            onChangeText={(input) => setPassword(input)}
          />
          <Button
            buttonStyle={{
              width: "50%",
              alignSelf: "center",
              margin: 2,
              backgroundColor: "white",
            }}
            title="Sign Up"
            titleStyle={{ color: "#3E205B" }}
            onPress={onSignUp}
          />
        </View>
      </LinearGradient>
    </View>
  );
}
