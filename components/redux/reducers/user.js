export const user = (state = { currentUser: null }, action) => {
  return {
    ...state,
    currentUser: action.currentUser,
  };
};

// const user = (state = null,action)=>{
//     return(action.currentUser)
// }

// export default user;
